<?php

use App\Support\ArrayHelper;

function array_get($array, $key, $default = null)
{
    return ArrayHelper::get($array, $key, $default);
}

function array_first($array, $callback = null, $default = null)
{
    return ArrayHelper::first($array, $callback, $default);
}

function array_last($array, $callback = null, $default = null)
{
    return ArrayHelper::last($array, $callback, $default);
}

function array_has($array, $key)
{
    return ArrayHelper::has($array, $key);
}

function array_where($array, $callback = null)
{
    return ArrayHelper::where($array, $callback);
}

function array_only($array, $key)
{
    return ArrayHelper::only($array, $key);
}

function array_forget(&$array, $keys)
{
    return ArrayHelper::forget($array, $keys);
}

function array_add(&$array, $key, $value)
{
    return ArrayHelper::add($array, $key, $value);
}