<?php

use App\Support;

require 'vendor/autoload.php';

$users = [
    ['name' => 'Shaun', 'score' => 60],
    ['name' => 'Bob', 'score' => 160],
    ['name' => 'Joe', 'score' => 123],
    ['name' => 'Billy', 'score' => 20],
    ['name' => 'Jess', 'score' => 65],
];

// $users = array_where($users, function($user, $key) {
//     return array_get($user, 'score') <= 100;
// });

// var_dump($users);

$user = [
    'name' => 'Shaun',
    'topics' => [
        ['title' => 'Hi arrays'],
        ['title' => 'Bye arrays'],
    ],
    'country' => [
        'name' => 'South Africa',
        'flag' => [
            'url' => 'kkk.png',
        ],
    ]
];

array_add($user, 'w', 30);
var_dump($user);